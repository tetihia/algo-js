class User {
    constructor() {
        this.name = '';
        this.firstName = '';
        this.id = '';
        this.password = '';
    }
}
/** 
 * unique(user:User,list:User[])
 * @param user:User Une variable de type User
 * @param list:User[] un tableau de user 
 * @returns boolean 
 **/

function unique(user, list) {
    let yesYouCan = true;
    for (let index = 0; index < list.length; index = index + 1) {
        if (list[index].id === user.id) {
            yesYouCan = false;
        }
    }
    return yesYouCan;

}
/**
 * 
 * @param {*} user Some user available
 * @param {*} list A list of users
 * @return User[]
 */
function ajouter(user, list) {
    if (unique(user, list)) {
        list.push(user);
    }
    return list;
}
/**
 * début de l'app admin
 */
let listUsers = [];

const user = new User();
user.name = 'o_O';
user.fistName = 'x_X'
user.id = 'x_O'
user.password = 'admin';

const user2 = new User();
user2.fistName = 'Ox_X'
user2.name = 'Oo_O';
user2.id = 'Ox_O'
user2.password = 'admin';

listUsers = ajouter(user, listUsers);
listUsers = ajouter(user2, listUsers);
listUsers.forEach((user) => {
    // console.log(`User :  ${user.name} : ${(user.id)}`);

});
// console.log(`items: ${listUsers.length}`);
// console.log(listUsers);

/**
 * Cryptage 
 * L'idée: Partir d'une chaine "admin" et obtenir qq chose de genre ")Dx1z"
 */
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index[0]) + replacement + this.substr(index[0] + replacement.length);
}

let word = "admin"
// word=word.replaceAt(0,"q");
// word=word.replaceAt(1,"e");
// word=word.replaceAt(2,"f");
// word=word.replaceAt(3,"g");
// word=word.replaceAt(4,"h");

console.log("word : " + word)

for (let i = 0; i < word.length; i++) {
    console.log("iteration i: " + word[i])
    console.log([i]);

    switch (word[i]) {
        case "a":
            word = word.replaceAt([i], "q");
            console.log("q");
            break;
        case "b":
            word = word.replaceAt([i], "s");
            console.log("s");
            break;
        case "c":
            word = word.replaceAt([i], "d");
            console.log("d");
            break;
        case "d":
            word = word.replaceAt([i], "f");
            console.log("f");
            break;
        case "e":
            word = word.replaceAt([i], "g");
            console.log("g");
            break;
        case "f":
            word = word.replaceAt([i], "h");
            console.log("h");
            break;
        case "g":
            word = word.replaceAt([i], "j");
            console.log("j");
            break;
        case "h":
            word = word.replaceAt([i], "k");
            console.log("k");
            break;
        case "i":
             word = word.replaceAt([i], "l");
            // console.log(word)
            console.log("l");
            break;
        case "j":
            word = word.replaceAt([i], "m");
            console.log("m");
            break;
        case "k":
            word = word.replaceAt([i], "a");
            console.log("a");
            break;
        case "l":
            word = word.replaceAt([i], "z");
            console.log("z");
            break;
        case "m":
            
            console.log("e");
            word = word.replaceAt([i], "e");
            // console.log(word)
            break;
        case "n":
             word = word.replaceAt([i], "r");
            // console.log(word)
            console.log("r");
            break;
        case "o":
            word = word.replaceAt([i], "t");
            console.log("t");
            break;
        case "p":
            word = word.replaceAt([i], "y");
            console.log("y");
            break;
        case "q":
            word = word.replaceAt([i], "u");
            console.log("u");
            break;
        case "r":
            word = word.replaceAt([i], "i");
            console.log("i");
            break;
        case "s":
            word = word.replaceAt([i], "o");
            console.log("o");
            break;
        case "t":
            word = word.replaceAt([i], "p");
            console.log("p");
            break;
        case "u":
            word = word.replaceAt([i], "w");
            console.log("w");
            break;
        case "v":
            word = word.replaceAt([i], "x");
            console.log("x");
            break;
        case "w":
            word = word.replaceAt([i], "c");
            console.log("c");
            break;
        case "x":
            word = word.replaceAt([i], "v");
            console.log("v");
            break;
        case "y":
            word = word.replaceAt([i], "b");
            console.log("b");
            break;
        case "z":
            word = word.replaceAt([i], "n");
            console.log("n");
            break;

        default:
            console.log(word);
    }

}
console.log("END: " + word)