/**
 * main
 * @author Moi <moi@moi.com>
 * Simple approche algorothmique
 * -def des variables
 * -affectations de valeurs aux variables
 * -les différents types scalaires
 * 
 * @version 1.0.0 
 */

let myName='';
myName="LeName";
yourName="Bond";



console.log(myName);
myName="test";
console.log(myName);

let isNewVariable=false;
// isNewVariable='cestUnBoolean'; pb ici car ecrase la valeur
console.log(isNewVariable);

// affectation d'une variable avec une autre variable

myName=yourName;
yourName='superman'
console.log(`${myName} <=> ${yourName}`);

let users=['Clad','Genesis','Aerith','Sephiroth','Zack'];
console.log(users)

// for( let index=0;index<4;index=index+1)
console.log("----------for :----------")
for(let i=0;i<users.length;i++){
    console.log(users[i])
}

index=0;
console.log("----------while  :----------")
while(index<users.length){
        console.log(users[index])
    index=index +1
}
console.log("--------do --while  :----------")
do{
    console.log(users[index]);
    index=index+1;
} while(index<users.length);

if (users[0]=='clad'){
    console.log(`Hello ${users[0]}`)
}
else{
    console.log(`tes pas clad, du coup salut ${users[0]}`)
}

if (users[3]='aubert'){
    console.log(`hola ${users[3]}`)
}
// <,>,>=,<= 
/* <> ( ou !=) */
let isTrue;
console.log("istrue: "+isTrue);
if(isTrue){
    console.log("je suis vrai");
}
else{
    console.log("je suis faux");
}

//ternaire

// let isJl= users[0]==='Clad'? true:false;
users[0]==='Clad'? console.log('okay cest sa '):console.log('nop cest faux ');
